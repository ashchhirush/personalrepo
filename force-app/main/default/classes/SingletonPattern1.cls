public with sharing class SingletonPattern1 {
    private static SingletonPattern1 sm;

    private SingletonPattern1() {

    }
    public static SingletonPattern1 getInstance(){
        if(sm==null)
            sm=new SingletonPattern1();
        return sm;
    }
}