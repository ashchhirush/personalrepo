public class FistClass{

    public static LowerWrapperClass getData(){
        
        LowerWrapperClass lc=new LowerWrapperClass();
        lc.count=2;
        lc.msg='Test Wrapper Class';
        return lc;
    } 

    public class LowerWrapperClass{
        public integer count;
        public string msg;
    }
}