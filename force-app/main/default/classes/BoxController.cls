public class BoxController {

    @TestVisible private static String SETTING_NAME = '0052x000001Jcw2';

    @TestVisible private static String ACCESS_TOKEN_URL = 'https://api.box.com/oauth2/token';

    @TestVisible private static String AUTHORIZE_URL = 'https://api.box.com/oauth2/authorize';

    @TestVisible private static String FOLDER_URL = 'https://api.box.com/2.0/folders';

    @TestVisible private String access_token;
    @TestVisible private Boolean isCallback;
   
    /** The JSON result from a successful oauth call */
    public class OAuthResult {
        /** The access token */
        public String access_token {get; set;}
        /** The refresh token */
        public String refresh_token {get; set;}
    }
    /**
020
    * Validates the oauth code
021
    *
022
    * @param code The code to validate
023
    * @param redirect_uri The URL to redirect to after successful validation
024
    * @return The oauth result
025
    */
    public static OAuthResult validateCode(String code, String redirect_uri) {
        String client_id = BoxCCredentials__c.getValues(SETTING_NAME).Client_Id__c;

       String client_secret = BoxCCredentials__c.getValues(SETTING_NAME).Client_Secret__c;


        List<String> urlParams = new List<String> {
            'grant_type=authorization_code',
            'code=' + EncodingUtil.urlEncode(code, 'UTF-8'),
            'client_id=' + EncodingUtil.urlEncode(client_id, 'UTF-8'),
            'client_secret=' + EncodingUtil.urlEncode(client_secret, 'UTF-8'),
            'redirect_uri=' + EncodingUtil.urlEncode(redirect_uri, 'UTF-8')

        };

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(ACCESS_TOKEN_URL);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('Accept', 'application/json');
        String body = String.join(urlParams, '&');
        req.setBody(body);
        HttpResponse res = h.send(req);
        return (OAuthResult)(JSON.deserialize(res.getBody(), OAuthResult.class));
    }

    /**
053
    * Generic constructor
054
    */

    public BoxController() {
       this.isCallback = ApexPages.currentPage().getParameters().containsKey('code');
    if (BoxCCredentials__c.getValues(SETTING_NAME) != null) {
            this.access_token = BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
        }
    }

    /**
064
    * Gets the authroization URL
065
    *
066
    * @return The authorization url
067
    */

    public String getAuthUrl() {
    System.debug('i am here');
        Map<String, String> urlParams = new Map<String, String> {
            'client_id' => BoxCCredentials__c.getValues(SETTING_NAME).Client_Id__c,
            'redirect_uri' => getPageUrl(),
            'response_type' => 'code'
        };
        System.debug(AUTHORIZE_URL);
        PageReference ref = new PageReference(AUTHORIZE_URL);
        ref.getParameters().putAll(urlParams);
        return ref.getUrl();

    }

    /**
082
    * Gets the page url
083
    *
084
    * @return The page url
085
    */
    @testVisible
    private String getPageUrl() {
        String host = ApexPages.currentPage().getHeaders().get('Host');
       
         String path = ApexPages.currentPage().getUrl().split('\\?').get(0);
        return 'https://' + host + path;
    }

     

    /**
095
    * If the access token is set
096
    *
097
    * @return If the access token is set
098
    */

    public Boolean getHasToken() {
        return (this.access_token != null);
    }

    /**
104
    * Validates the callback code and generates the access and refresh tokens
105
    *
106
    * @return null to refresh the page
107
    */
    public PageReference redirectOnCallback() {
        System.debug('redirectoncallback--------------');
        if (this.isCallback) {
            String code = ApexPages.currentPage().getParameters().get('code');
            System.debug('i am here2'+code);
            OAuthResult result = validateCode(code, this.getPageUrl());
            System.debug(result);        
            BoxCCredentials__c creds = BoxCCredentials__c.getValues(SETTING_NAME);
            creds.Access_Token__c = result.access_token;
            creds.Refresh_Token__c = result.refresh_token;
           update creds;
        }
        return null;

    }

     

    private class ParentFolder {
        public String id;
        public ParentFolder(String id) {
            this.id = id;
        }
    }

    private class Folder {
        public String name;
        ParentFolder parent;
        public Folder(String name, String parentId) {
            this.name = name;
            this.parent = new ParentFolder(parentId);
        }
    }

     

    /**
142
    * Static method to create the folder inside of the box account
143
    *
144
    * @param accountId The account id to create the folder for
145
    */

    @Future(callout = true)
    public static void createFolder(Id accountId) {
        if (BoxCCredentials__c.getValues(SETTING_NAME) == null) {
            System.debug('I am here');
            return;
        }
        String access_token = BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
        Folder folder_info = new Folder(accountId, '0');
        HttpRequest request=new HttpRequest();
        request.setEndpoint(FOLDER_URL);
        request.setMethod('POST');
        request.setHeader('Authorization', 'Bearer ' + access_token);
        String body = JSON.serialize(folder_info);
        System.debug(body);
        request.setBody(body);
        Http p = new Http();
        HttpResponse response = p.send(request);
        System.debug(response);
    }
}