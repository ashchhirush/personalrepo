global class schedulesubBatch implements Schedulable{

    public static String sched = '0 00 00 * * ?';  //Every Day at Midnight 

    global static String scheduleMe() {
        schedulesubBatch SC = new schedulesubBatch(); 
        return System.schedule('My batch Job', sched, SC);
    }

    global void execute(SchedulableContext sc) {
          SubBatchApex bc=new SubBatchApex(); 
        ID batchprocessid = Database.executeBatch(bc);           
    }
}