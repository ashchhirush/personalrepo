public class SecondTaskController {
   
    public String FName{get; set;}
    public String LName{get;set;}
    public String emailcontact{get;set;}
    public String titlecontact{get;set;}
    public Account account{get;set;}
    public Contact contact{get;set;}
    
    public ContactDataStore__c reqcontact{
        get;
        set;
    }
    public  List <ContactDataStore__c > lstcon {
        get;
        set;
    }
    ApexPages.StandardController sc;
    public String accId;
    public SecondTaskController(ApexPages.StandardController sc){
      
           
             account = new Account();
              accId = sc.getId();     
               helper();
        
    }
    public void helper(){
            Account account1 = [select id, name, Type, Phone, Industry from Account where id =: accId];
            Account.Name = account1.Name;
            Account.Type = account1.Type;
            Account.Industry = account1.Industry;
            Account.Phone = account1.Phone;
            lstcon = [select id,FirstName__c, LastName__c, Email__c, Title__c  from ContactDataStore__c where AccountName__C =: account1.Name ];
    }
    
    public PageReference saveAccount(){
        List <ContactDataStore__c> cc=[select id,FirstName__c, LastName__c, Email__c, Title__c  from ContactDataStore__c where AccountName__C =: Account.Name ];
        for(ContactDataStore__c cins : cc){
            
        }
        
        return null;
    }
    
    public PageReference saveContact(){
        try{
             ContactDataStore__c cc=new ContactDataStore__c();
             cc.AccountName__c=Account.Name;
             cc.FirstName__c=FName;
             cc.LastName__c=LName;
             cc.Title__c=titlecontact;
             cc.Email__c=emailcontact;
             insert(cc);

             return null;
        }
        catch(exception ex){
        return null;
        }
        
    }
     
 public List < ContactDataStore__c > getreqContact() {
        try {
            
            lstcon = [select id,FirstName__c, LastName__c, Email__c, Title__c  from ContactDataStore__c ];
                        return lstcon;
        } catch (exception ex) {
            
            return null;
        }
    }

    
}