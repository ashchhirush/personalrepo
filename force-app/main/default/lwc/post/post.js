import { LightningElement,api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class Post extends NavigationMixin(LightningElement) {

    @api question;
    connectedCallback(){
        console.log(JSON.stringify(this.question));
    }

    openQuestionModel(){
        this.handleNavigate();
    }

    /*handleNavigate() {
        this[NavigationMixin.Navigate]({
            type: "standard__component",
            attributes: {
                componentName: "c__PostNavigator"
            },
            state: {
                //c__propertyValue: this.question
            }
        });
    }*/


    /*handleNavigate() {
        var compDefinition = {
            componentDef: "c:queAnsModel",
            attributes: {
                question: this.question
            }
        };
        // Base64 encode the compDefinition JS object
        var encodedCompDef = btoa(JSON.stringify(compDefinition));
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: '/one/one.app#' + encodedCompDef
            }
        });
    }*/

    handleNavigate(){
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'postmodel'
            },
            state: {
                'question':this.question.Id
               }
        });
        
    }
}