public class savePDF {
    Date myDate = system.today();
    public Opportunity pageOpp{get;set;}
    public Boolean initialised{get; set;}
    public Account a{get;set;}
    public savePDF(ApexPages.StandardController stdController) {
        this.pageOpp = (Opportunity)stdController.getRecord();
        id PageOppId = ApexPages.currentPAge().getParameters().get('id');
        Opportunity op=[select id ,billed_to__c,accountid from opportunity where id =:pageoppid];   
        if(string.isblank(op.Billed_To__c)){
            a=[select id,name,billingstreet,billingstate,billingcity,billingcountry,billingAddress,parentid
               from account where id=: op.Accountid ];
            if(a.BillingAddress==null){
                a=[select id,name,billingstreet,billingstate,billingcity,billingcountry
                   from account where id=: a.ParentId ];
            }   
        }
        else{
            
            a=[select id,name,billingstreet,billingstate,billingcity,billingcountry
               from account where id=: op.Billed_To__c ];
        }
        initialised=false;
        system.debug('id = ' + pageOpp.id + ' id2' +PageOppId);
    }
    public pagereference saveAttachement() {
         PageReference retURL;
        if (!initialised && pageOpp.ID !=NULL) {
            PageReference pdf = Page.PDFPage;
            Attachment attach = new Attachment();           
            Blob body = pdf.getContentAsPDF();
            attach.Body = body;
            attach.Name = opportunity.name + ' SOF'+ myDate + '.pdf';
            attach.IsPrivate = false;
            attach.ParentId = pageOpp.Id;
            insert attach;
            system.debug(attach);
            initialised=true;
               retURL = new PageReference('/'+ pageOpp.id);
        } else {system.debug('tried to run twice');
               return null;
               }
      
        return returl;
    }
}