public without sharing class QuesAnswController {

    @AuraEnabled (cacheable=true)
    public static List<Question__c> getPosts(){
        List<Question__c> posts;
        try {
             posts=[select
                        id,
                        Question_Text__c,
                        Answer_Type__c,
                        Created_By__c,
                        createdDate,
                        (select 
                            id,
                            Answer_Type__c,
                            Answer_Text__c,
                            Answered_By__c,
                            createddate 
                            from Answers__r order by createddate desc
                        )
                        from Question__c order by createddate desc limit 50000];
            return posts;
        } catch (Exception e) {
            system.debug('Error in fetch records');
            return posts;
        }
    }


    @AuraEnabled (cacheable=true)
    public static List<Question__c> getPost(string recID){
        List<Question__c> post;
        try {
             post=[select
                        id,
                        Question_Text__c,
                        Answer_Type__c,
                        Created_By__c,
                        createdDate,
                        (select 
                            id,
                            Answer_Type__c,
                            Answer_Text__c,
                            Answered_By__c,
                            createdDate 
                            from Answers__r order by createddate desc
                        )
                        from Question__c  where id=:recID];
            return post;
        } catch (Exception e) {
            system.debug('Error in fetch records');
            return post;
        }
    }


}