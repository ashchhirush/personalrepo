public class AccountController {
    public Contact contact {
        get;
        set;
    }
    public Account account {
        get;
        private set;
    }
    public String accId;


    List < contact > lstcon = new List < contact > ();

    ApexPages.StandardController sc;
    public AccountController(ApexPages.StandardController sc) {
        try {
            this.sc = sc;
            System.Debug('sc' + sc);
            account = new Account();
            accId = sc.getId();
            System.debug('accountid:1' + accId);
            contact = new Contact();
            processing();
        } catch (exception ex) {
            System.debug('Lineno---' + ex.getLineNumber());
            System.debug('Error' + ex.getMessage());
            System.debug('Cause' + ex.getCause());
            System.debug('stacktrace' + ex.getStackTraceString());
        }
    }
    public void processing() {
        try {
            System.debug('accountid:' + accId);
            Account account1 = [select id, name, Type, Phone, Industry from Account where id =: accId];
            Account.Name = account1.Name;
            Account.Type = account1.Type;
            Account.Industry = account1.Industry;
            Account.Phone = account1.Phone;
        } catch (exception ex) {
            System.debug('Lineno---' + ex.getLineNumber());
            System.debug('Error' + ex.getMessage());
            System.debug('Cause' + ex.getCause());
            System.debug('stacktrace' + ex.getStackTraceString());


        }
    }

    public List < contact > getContacts() {
        try {
            lstcon.clear();
            // Account ac=[select id,name from Account where name=:Account.Name];
            lstcon = [select id, name, Phone, email, accountId, Title, Department, Description from contact where accountid =: accId];
            system.debug('### List of Contacts for Test is ###' + lstcon);
            return lstcon;
        } catch (exception ex) {
            System.debug('Lineno---' + ex.getLineNumber());
            System.debug('Error' + ex.getMessage());
            System.debug('Cause' + ex.getCause());
            System.debug('stacktrace' + ex.getStackTraceString());
            return null;
        }
    }

    public PageReference saveContact() {
        try {
            System.debug(contact);
            System.debug('iddf' + accId);
            contact.AccountId = accId;
            insert(contact);
            contact = new Contact();
            return null;
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            System.debug('Lineno---' + ex.getLineNumber());
            System.debug('Error' + ex.getMessage());
            System.debug('Cause' + ex.getCause());
            System.debug('stacktrace' + ex.getStackTraceString());
            return null;
        }

    }


    public PageReference saveAccount() {
        try {

            Account account1 = [select id from Account where id =: accId];
            account1.Name = Account.Name;
            account1.Phone = Account.Phone;
            account1.Type = Account.Type;
            account1.Industry = Account.Industry;

            System.debug('before updation:' + account1);
            update(account1);
            System.debug('after updation' + account1);
            //helpController.addContact(account.Name,contact.FirstName,contact.LastName,contact.Title,contact.Email);
            return null;
        } catch (System.DMLException ex) {
            ApexPages.addMessages(ex);
            System.debug('Lineno---' + ex.getLineNumber());
            System.debug('Error' + ex.getMessage());
            System.debug('Cause' + ex.getCause());
            System.debug('stacktrace' + ex.getStackTraceString());
            return null;
        }

    }

}