public class AccountTriggerHandler {

    
    
    public static void updateCaseDescription(List<Account> newList , Map<id,Account> OldAccount){
        Set<id> accId=new set<id>();
        For(Account ac:newList){
            accId.add(ac.id);
        }
        system.debug(accId);
        Map<id,Case> allCases=new map<id,case>([select id,description,accountid from case where accountid in:accId order by CreatedDate desc limit 1]);
        
        Map<id,Case> accWithCaseMap=new Map<id,Case>();
        
        for(Case cs:allCases.values()){
            accWithCaseMap.put(cs.accountid,cs);
        }
        
        List<Case> finalCaseList=new list<Case>();
        for(Account ac:newList){
            
            if(accWithCaseMap.containsKey(ac.id)){
                Case cs=accWithCaseMap.get(ac.id);
                if(string.isblank(cs.Description)){
                    cs.description=system.now()+'\n'+'description==>'+ac.Description;
                }
                else{
                    cs.description+='\n'+system.now()+'\n'+'description==>'+ac.Description;
                }
                finalCaseList.add(cs);
            }
            else{
                //create a case
                Case cs=new Case();
                cs.AccountId=ac.id;
                cs.description=system.now()+'\n'+'description==>'+ac.Description;
                finalCaseList.add(cs);
            }
            
            
        }
        
        system.debug(finalCaseList);
        upsert finalCaseList;
        
        
        
    }
}