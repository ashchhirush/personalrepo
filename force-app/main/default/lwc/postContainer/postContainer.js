import { LightningElement,wire,api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getAllPosts from '@salesforce/apex/QuesAnswController.getPosts';
export default class PostContainer extends NavigationMixin(LightningElement) {

    @wire(getAllPosts)
    posts;

    openQuestionModel(){
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'askquestion'
            },
            state:{}
        });
    }

}