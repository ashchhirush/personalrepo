import { LightningElement,track,api } from 'lwc';

export default class CalanderDaysWrapper extends LightningElement {

    @track
    days=[];
    @api incomingMonth;
    @api incomingYear;

    connectedCallback(){
        console.log(this.incomingMonth + this.incomingYear);
        this.fillDaysArray(this.incomingMonth,this.incomingYear);
    }

    @api
    fillDaysArray(incomingMonth,incomingYear){

        let dateString=incomingMonth + '1, '+incomingYear;
        let stday=new Date(dateString).getDay();
        console.log(stday );
        let totalDays=this.dasInMonth(incomingMonth,incomingYear);
        console.log(totalDays);
        this.putdaysData(stday,totalDays);
    }

    putdaysData(stday,totalDays){
        
        let daysArray=[];
        for(let i=0;i<stday;i++){
            daysArray.push({visibility:false,day:0});
        }
            
        for(let i=1; i<=totalDays; i++){
            daysArray.push({visibility:true,day:i});
        }
        console.log(daysArray);
        this.days=daysArray;
    }

    dasInMonth (month, year) {
        let mCode=this.getMonthCode(month);
        return new Date(year, mCode, 0).getDate();
    }

    getMonthCode(month){
        if(month=='January'){
            return 1;
        }
        if(month=='February'){
            return 2;
        }
        if(month=='March'){
            return 3;
        }
        if(month=='April'){
            return 4;
        }
        if(month=='May'){
            return 5;
        }
        if(month=='June'){
            return 6;
        }

        if(month=='July'){
            return 7;
        }
        if(month=='August'){
            return 8;
        }
        if(month=='September'){
            return 9;
        }
        if(month=='October'){
            return 10;
        }
        if(month=='November'){
            return 11;
        }
        if(month=='December'){
            return 12;
        }

    }
}