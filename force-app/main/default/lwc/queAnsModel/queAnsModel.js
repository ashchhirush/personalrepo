import { LightningElement,api, wire,track } from 'lwc';
import getPost from '@salesforce/apex/QuesAnswController.getPost';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import ANSWER_FIELD from '@salesforce/schema/Answer__c.Answer_Text__c';
import { refreshApex } from '@salesforce/apex';

export default class QueAnsModel extends LightningElement {
    questionId;
    isAnswerMode=false;
    @api objectApiName='Answer__c';

    fields = [ANSWER_FIELD];

    connectedCallback() {
        let testURL = window.location.href;
        let newURL = new URL(testURL).searchParams;
        console.log('id ===> '+JSON.stringify(newURL.get('question')));
        this.questionId = newURL.get('question');    
    }

    @wire(getPost , { recID: '$questionId' })
    question;
    
    openAnswerModel(){
        this.isAnswerMode=true;
    }

    closeAnsModel(){
        this.isAnswerMode=false;
    }

    onSubmitHandler(event) {
        event.preventDefault();
        const fields = event.detail.fields;
        fields.Question__c = this.questionId;
        // You need to submit the form after modifications
        this.template.querySelector('lightning-record-form').submit(fields);
    }    

    handleAnsSubSuccess(event){
        this.isAnswerMode=false;
        const evt = new ShowToastEvent({
            title: 'Answer is created',
            message: '',
            variant: 'success',
        });
        this.dispatchEvent(evt);
        refreshApex(this.question);
    }


}