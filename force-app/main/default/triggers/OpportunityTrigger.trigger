trigger OpportunityTrigger on Opportunity (before insert,before update) {
    
    If(trigger.isInsert){
        OpportunityTriggerHandler.counter=false;
        OpportunityTriggerHandler.insertopty(trigger.new);
    }
    if(trigger.isupdate){
        if(OpportunityTriggerHandler.counter==true){
            OpportunityTriggerHandler.counter=false;
            OpportunityTriggerHandler.showDelete(trigger.new);
        }
    }

}