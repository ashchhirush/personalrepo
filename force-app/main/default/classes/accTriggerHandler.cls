public class accTriggerHandler {

    public static void industrialStrengthHandleTrigger(List<Account> workOrderList,Map<id,Account> newMap,Map<id,Account> oldMap,TriggerOperation operation){
        Boolean translationNeeded=false;
        for(Account wo:workOrderList){
            if(operation==TriggerOperation.BEFORE_INSERT || wo.Description!=oldMap.get(wo.id).Description){
                wo.TranslationPending__c=true;
                translationNeeded=true;
            }
        }
        if(translationNeeded)
            industrialStrengthRequestAsync();
    }
    
    public static void industrialStrengthRequestAsync(){
        if(System.isFuture() || system.isBatch() || system.isQueueable()){
            industrialStrengthSync();
        }else{
            if(Limits.getFutureCalls()<Limits.getLimitFutureCalls()-3){
                industrialStrengthAsync();
            }
        }
        
    }

    @future(callout=true)
    public static void industrialStrengthAsync(){
        industrialStrengthSync();
    }

    public static void industrialStrengthSync(){
        Integer allowedCallouts=Limits.getLimitCallouts()-Limits.getCallouts();
        if(allowedCallouts<=0)
            return;
        List<Account> workOrdersToUpdate=[select id ,Description,DescriptionSpanish__c from Account
                                            where TranslationPending__c =true Limit : allowedCallouts];
        if(workOrdersToUpdate.size()==0)
            return;
        for(Account wo:workOrdersToUpdate){
            wo.DescriptionSpanish__c='abc';
            wo.TranslationPending__c=false;
        }
        update workOrdersToUpdate;
    }
    
    @future
    public static void insertAcc(){
        Account ac=new Account(description='ss',name='ss',industry='Agriculture');
        insert ac;
    }
}