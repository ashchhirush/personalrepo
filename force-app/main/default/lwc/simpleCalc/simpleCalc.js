import { LightningElement } from 'lwc';

export default class SimpleCalc extends LightningElement {

    firstNo;
    secondNo;
    res;
    firstNumberChange(event){
        this.firstNo=event.target.value;
    }
    secondNumberChange(event){
        this.secondNo=event.target.value;
    }

    add(){
        this.res= parseInt(this.firstNo) + parseInt( this.secondNo); 
    }
    sub(){
        this.res= this.firstNo-this.secondNo;
    }
    mul(){
        this.res= this.firstNo*this.secondNo;
    }
    div(){
        this.res= this.firstNo/this.secondNo;
    }

}