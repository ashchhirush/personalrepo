public class MyCalloutTest {
    
    public static void makeCall(){
        Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals');
		request.setMethod('GET');
		HttpResponse response = http.send(request);
		// If the request is successful, parse the JSON response.
		system.debug(response);
        if(response.getStatusCode()==200){
            System.debug(response.getBody());
            Map<String,object> a=new Map<String,object>();
            a= (Map<string,object>)json.deserializeUntyped(response.getBody());
            System.debug('a'+a);
            
            System.debug('animals-----'+ a.get('animals'));
            List<object> b=(list<object>)a.get('animals');
            for (Object c:b){
                System.debug(c);
            }
            
        }
    }
    
    public static HttpResponse getResponse(){
        
        Http http=new Http();
        
        HttpRequest req=new HttpRequest();
        
        req.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody('{"name":"mighty mouse"}');
        
        HttpResponse res=http.send(req);
        System.debug(res);
        if(res.getStatusCode()==201){
            System.debug(res.getBody());
            Map<String,Object> s=(Map<String,Object>) json.deserializeUntyped(res.getBody());
            System.debug(s);
            List<object> a=(List<object>)s.get('animals');
            for(object b:a){
                System.debug(b);
            }
        }
        return res;
        
    }

}