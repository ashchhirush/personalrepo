global class UpdateCaseTeamMemberOnCaseBatchClass implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        //Id recordtypeI =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('FinCrim Case').getRecordTypeId();
        //Id recordtypeII =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Financial Crime Notification').getRecordTypeId();
        String query = 'Select id,member.name,memberid ,Parentid from CaseTeamMember';
        return Database.getQueryLocator(query);
    }   
    global void execute(Database.BatchableContext BC, List<CaseTeamMember> caseList) {
        
    	String mem;
        List<CaseTeamMember> caseTmMemList =new list<CaseTeamMember>();
        for(CaseTeamMember c : caseList){
            mem=c.memberid;
            if(mem.startsWith('005')){
                caseTmMemList.add(c);
            }
            
        }
        if(caseTmMemList != null && caseTmMemList.size() > 0) {
            delete caseTmMemList;
        }
     }   
    
    global void finish(Database.BatchableContext BC) {     
   
    }

}