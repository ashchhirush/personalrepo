({
    handlePEvent : function(component, event, helper) {
        alert('app event handled by parent');
    },
    fireEventP: function(component,event,helper){
        var s=$A.get("e.c:InterviewPrepApplicationEvent");
        s.fire();

    },
    handlePEvent2:function(component,event){
        alert('cmp event handled by parent');

    },

    callChild:function(component,event){
        var ch=component.find("childCmp1");
        var msg=ch.childMthod('hi','child');
        alert(msg);

    }
})