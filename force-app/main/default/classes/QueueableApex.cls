public class QueueableApex implements Queueable, Database.AllowsCallouts, Finalizer  {
    
    //trigger will run on after insert/update
    //we will insert data in a seperate object with ids of all records which are to be make any callout
    //on insert of that record we will call a queue
    //we will initialize finalizer in execute method
    //we will try to make callouts here
    //we will check callouts limits and if not then return false success
    //if not succedded then we will try to execute other queue from the existing one
    // on error also we will try to again execute 
    
    @TestVisible
    private static boolean fakeException;
    private ID currentAsyncRequestId;
    public static void handleTriggerWithQueueable(List<Account> workOrderList,Map<ID,Account> newMap,Map<id,Account> oldMap,TriggerOperation operation){
        List<AsyncRequest__c> newAsyncRequests=new list<AsyncRequest__c>();
        List<string> textChangeIDs=new List<ID>();
        Integer maxIdsPerRequest=Limits.getLimitQueueableJobs();
        for(Account wo:workOrderList){
        	if(operation==TriggerOperation.AFTER_INSERT||wo.Description!=oldMap.get(wo.id).Description){
                textChangeIDs.add(wo.id);
        	}
            if(textChangeIDs.size()>maxIdsPerRequest){
                newAsyncRequests.add(new AsyncRequest__c(AsyncType__c='Translate Work Order',Params__c=string.join(textChangeIDs,',')));
            	textChangeIDs.clear();
            }
        }
        
        if(textChangeIDs.size()>0){
            newAsyncRequests.add(new AsyncRequest__c(AsyncType__c='Translate Work Order',Params__c=string.join(textChangeIDs,',')));
        }
        
        if(newAsyncRequests.size()>0){
			insert newAsyncRequests;            
        }
        
    }
   
    public static ID startQueueable(ID currentJobID){
        
        List<AsyncApexJob> jobs=[select id, status,ExtendedStatus from AsyncApexJob
                                where jobType='Queueable' and (status='Queued' or status='Holding')
                                and createdbyid=:userinfo.getUserId() and apexclass.name='QueueableApex'];
        if(jobs.size()>=1|| (jobs.size()==1 && jobs[0].id !=currentJobID )){
            return null;
        }
        
        if(Limits.getLimitQueueableJobs()-Limits.getQueueableJobs()>0)
            return system.enqueueJob(new QueueableApex());
        else 
            return null; 
    }
    
    
    public static void execute(QueueableContext context){
        //On off switch
        
        QueueableApex finalizer=new QueueableApex();
        system.attachFinalizer(finalizer);
        List<AsyncRequest__C> requests;
        try {
            requests=[select id,AsyncType__c,Params__c from AsyncRequest__C where AsyncType__c='Translate Work Order'
                     and createdbyid=:userinfo.getUserId() and error__c=false limit 1];
        }catch(Exception ex){return;}
        if(requests.size()==0){
            return;            
        }
        
        AsyncRequest__c currentRequest=requests[0];
        finalizer.currentAsyncRequestId=currentRequest.id;
        Boolean success=false;
        try{
            
            if(currentRequest.AsyncType__c=='Translate Work Order'){
				success=translate(currentRequest.id);                
            }
            if(success){
                List<AsyncRequest__c> relockRequests=[select id from AsyncRequest__c where error__c=false and id=:currentRequest.id for update ];
                
                if(relockRequests.size()==0){
                    return;
                }
                delete currentRequest;
                database.emptyRecycleBin(currentRequest);
            }
            
            if(fakeException){
                integer x=0;
                integer y=5/x;
            }
            if(!isAsyncRequestPending(currentRequest.id)){
                return;
            }
            
            try{
                startQueueable(context.getjobid());
            }catch(exception ex){
                tryToQueue();
            }
            
        }catch(Exception ex){
            currentRequest.Error__c=true;
            currentRequest.Error_Message__c=ex.getMessage();
            update currentRequest;
        }
    }
    
    public static void execute(FinalizerContext ftx){
        System.ParentJobResult result=ftx.getResult();
        if(result==ParentJobResult.UNHANDLED_EXCEPTION){
            System.debug(ftx.getException());
        }
    }
    
    public static boolean translate(id translationId){
        return true;
    }
    
    public static boolean isAsyncRequestPending(id currentRequestId){
        
        List<AsyncRequest__c> otherRequests=[select id from Asyncrequest__c where id<>:currentRequestId
                                            and error__c=false and createdbyid=:userinfo.getuserId()];
        if(otherRequests.size()>0)
            return true;
        else
            return false; 
    }
    
    @future
    private static void tryToQueue()
    {
        // On/off switch
        try {
            startQueueable(null);
        }
        catch(Exception ex)
        {
            // Wait for someone else to make a request...
            // Or maybe use scheduled Apex?
        }
    }    
    
    
    
    
}