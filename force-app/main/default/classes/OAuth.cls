global class OAuth {

	//private OAuth_Service__c service;
	private String token='';
	private String tokenSecret='';
	private Boolean isAccess;
	private String verifier;

	private String nonce=String.valueOf(Crypto.getRandomLong());
	private String timestamp=String.valueOf(DateTime.now().getTime()/1000);
	private String signature;
	private String realm='';
	private String consumerKey='';
	private String consumerSecret='';
	private String oauthversion;
   string signatureMethod = 'HMAC-SHA1';
    string version = '1.0';
	private Map<String,String> parameters = new Map<String,String>();
	
	public String message { get; set; }
	/**
	 * Looks up service name and starts a new authorization process
	 * returns the authorization URL that the user should be redirected to
	 * If null is returned, the request failed. The message property will contain
	 * the reason.
	 */	
	/*public void newAuthorization(String serviceName) {

		service = [SELECT realm__c, request_token_url__c, access_token_url__c, consumer_key__c, 
						  consumer_secret__c, authorization_url__c,
						  (select token__c, secret__c, isAccess__c FROM tokens__r WHERE owner__c=:UserInfo.getUserId() ) 
						  FROM OAuth_Service__c WHERE name = :serviceName];
		
		if(service==null) {
			System.debug('Couldn\'t find Oauth Service '+serviceName);
			message = 'Service '+serviceName+' was not found in the local configuration';
		}
				
		Http h = new Http();
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(service.request_token_url__c);
		System.debug('Request body set to: '+req.getBody());
		realm = service.Realm__c;
		consumerKey = service.consumer_key__c;
		consumerSecret = service.consumer_secret__c;
		token = service.Tokens__r[0].isAccess__c ? service.Tokens__r[0].Token__c : '';
		tokenSecret = service.Tokens__r[0].isAccess__c ? service.Tokens__r[0].Secret__c : '';
		sign(req);
		HttpResponse res = null;
		if(serviceName=='test1234') {
			// testing
			res = new HttpResponse();
		} else {
			try{
				res = h.send(req);
				system.debug('req '+req.getHeader('Authorization'));
				system.debug('res '+res);
				system.debug('resp body '+res.getBody());
				callOutHandler.createLog(req.getEndpoint(), req.getMethod(), res.getStatusCode(), req.getHeader('Content-Type'), req.getBody(), res.getBody());
				callOutHandler.routeResponse(res.getBody(), 'netsuite');
            
			}
			catch(exception e) {
				system.debug('exception '+e);
				callOutHandler.createLog(req.getEndpoint(), req.getMethod(), res.getStatusCode(), req.getHeader('Content-Type'), req.getBody(), res.getBody());
			}
		}
		System.debug('Response from request token request: ('+res.getStatusCode()+')'+res.getBody());
		if(res.getStatusCode()>299) {
			message = 'Request failed. HTTP Code = '+res.getStatusCode()+
					  '. Message: '+res.getStatus()+'. Response Body: '+res.getBody();
		}
	}

	public List<User> getUsersOfService(String serviceName) {
		List<OAuth_Token__c> l =
			[SELECT OAuth_Service__r.name, isAccess__c, Owner__r.name FROM OAuth_Token__c 
			 WHERE OAuth_Service__r.name= :serviceName AND isAccess__c = true];
			 
		List<User> result = new List<User>();
		for(OAuth_Token__c t : l) {
			result.add(t.owner__r);
		}
		return result;
	}

		*/
    


	private void refreshParameters() {
		parameters.clear();
		parameters.put('realm',realm);
		parameters.put('oauth_consumer_key',consumerKey);
		if(token!=null) {
			parameters.put('oauth_token',token);
		}

		if(verifier!=null) {
			parameters.put('oauth_verifier',verifier);
		}
		parameters.put('oauth_signature_method','HMAC-SHA1');
		parameters.put('oauth_version','1.0');
		parameters.put('oauth_timestamp',timestamp);
		parameters.put('oauth_nonce',nonce);
        System.debug('para in referes----'+parameters);
	}

	private Map<String,String> getUrlParams(String value) {

		Map<String,String> res = new Map<String,String>();
		if(value==null || value=='') {
			return res;
		}
		for(String s : value.split('&')) {
			System.debug('getUrlParams: '+s);
			List<String> kv = s.split('=');
			if(kv.size()>1) {
			  // RFC 5849 section 3.4.1.3.1 and 3.4.1.3.2 specify that parameter names 
			  // and values are decoded then encoded before being sorted and concatenated
			  // Section 3.6 specifies that space must be encoded as %20 and not +
			  String encName = EncodingUtil.urlEncode(EncodingUtil.urlDecode(kv[0], 'UTF-8'), 'UTF-8').replace('+','%20');
			  String encValue = EncodingUtil.urlEncode(EncodingUtil.urlDecode(kv[1], 'UTF-8'), 'UTF-8').replace('+','%20');
			  System.debug('getUrlParams:  -> '+encName+','+encValue);
			  res.put(encName,encValue);
			}
		}
		return res;
	}

	private String createBaseString(Map<String,String> oauthParams, HttpRequest req) {
		Map<String,String> p = oauthParams.clone();
		System.debug('it is needed----'+p);
		p.remove('realm');
		p.remove('deploy');
		p.remove('script');

		for(string k : oauthParams.keySet()){
			system.debug('oauthParams1-- '+k+' '+p.get(k));
		}

		if(req.getMethod().equalsIgnoreCase('post') && req.getBody()!=null && 
		   req.getHeader('Content-Type')=='application/x-www-form-urlencoded') {
		   	p.putAll(getUrlParams(req.getBody()));
		}
		String host = req.getEndpoint();
		Integer n = host.indexOf('?');
		if(n>-1) {
			p.putAll(getUrlParams(host.substring(n+1)));
			host = host.substring(0,n);
		}
		List<String> keys = new List<String>();
		keys.addAll(p.keySet());
		for(string k2 : keys) {
			system.debug('keys '+k2);
		}
		keys.sort();
		String s = keys.get(0)+'='+p.get(keys.get(0));
		for(Integer i=1;i<keys.size();i++) {
			s = s + '&' + keys.get(i)+'='+p.get(keys.get(i));
		}

		// According to OAuth spec, host string should be lowercased, but Google and LinkedIn
		// both expect that case is preserved.
		return req.getMethod().toUpperCase()+ '&' + 
			EncodingUtil.urlEncode(host, 'UTF-8') + '&' +
			EncodingUtil.urlEncode(s, 'UTF-8');
	}
	
	public String sign(HttpRequest req) {
		
		nonce = String.valueOf(Crypto.getRandomLong());
		timestamp = String.valueOf(DateTime.now().getTime()/1000);

		refreshParameters();
		System.debug('para in sign---'+parameters);
		String s = createBaseString(parameters, req);
		System.debug('Token Secret: '+tokenSecret);
		System.debug('Signature base string: '+s);
		
		Blob sig = Crypto.generateMac('HmacSHA1', Blob.valueOf(s), 
				       Blob.valueOf(consumerSecret+'&'+tokenSecret));	   
		signature = EncodingUtil.urlEncode(EncodingUtil.base64encode(sig), 'UTF-8');
		
		String header = 'OAuth ';
		for (String key : parameters.keySet()) {
			header = header + key + '="'+parameters.get(key)+'", ';
		}
		header = header + 'oauth_signature="'+signature+'"';
		System.debug('Authorization: '+header);
		//req.setHeader('Authorization',header);
        return header;
	}	
}