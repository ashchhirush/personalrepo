public class MyfirstController {
    
    
    
    public static void createPicklistJson(){
        
        List<string> objectList=new list<String>{'Case'};//getAllObjectList();
        DOM.Document doc = new DOM.Document();
        dom.XmlNode results = doc.createRootElement('Results', null, null);
        for(String obj:objectList){
            dom.XmlNode objNode = results.addChildElement('Object', null, null).addTextNode(obj);
            dom.XmlNode body1 = results.addChildElement('Fields', null, null);
            Map<String, Schema.SObjectField> objectFieldMap = Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap();
            for( String fName : objectFieldMap.keySet() ) {                
                Schema.DescribeFieldResult fDescription = objectFieldMap.get( fName ).getDescribe();
                Schema.DisplayType FldType = fDescription.getType();
                if(string.valueOf(FldType)== 'PICKLIST') {
                    dom.XmlNode body2 = body1.addChildElement('Field', null, null).addTextNode(fDescription.getName());
                    dom.XmlNode body3 = body1.addChildElement('ValueSet', null, null);
                    List<Schema.PicklistEntry> ple = fDescription.getPicklistValues();
                    for( Schema.PicklistEntry pickListVal : ple){
                        dom.XmlNode body4 = body3.addChildElement('Value', null, null);
                        dom.XmlNode fullName = body4.addChildElement('fullName', null, null).addTextNode(pickListVal.getValue());
                        dom.XmlNode lab = body4.addChildElement('label', null, null).addTextNode(pickListVal.getLabel());
                        dom.XmlNode def = body4.addChildElement('default', null, null).addTextNode(string.valueOf(pickListVal.isDefaultValue()));
                    }
                    
                }
                
            }
            
        }
        
        
        string xmlstring = doc.toXmlString();
        System.debug('xmlstring==> ' + string.valueOf(xmlString));
        
    }
    
    
    
    
    /*public static void getAllObjectList(){
Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
Set<String> standardObjects = new Set<String>();
Set<String> customObjects = new Set<String>();
for(Schema.SObjectType d : gd.values())
{
Schema.DescribeSObjectResult ds = d.getDescribe();
if(!ds.isCreateable())
continue;
if(ds.isCustom() == false && ds.getRecordTypeInfos().size() > 0)
standardObjects.add(ds.getName());
else if(ds.isCustom())
customObjects.add(ds.getName());
}
List<String> sortedNames = new List<String>(customObjects);
sortedNames.sort();
for(String name : sortedNames)
System.debug('Custom object: ' + name);
sortedNames = new List<String>(standardObjects);
sortedNames.sort();
for(String name : sortedNames)
System.debug('Standard object: ' + name);



}


*/
    
}