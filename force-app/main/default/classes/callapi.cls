public class callapi {

    public static HttpResponse calloutToNet(String endPoint,String bodyString,String methodName){
        
        HttpRequest httpRequestObject=new HttpRequest();
        httpRequestObject.setEndpoint(endpoint);
        httpRequestObject.setMethod(methodName);
        httpRequestObject.setTimeout(119990);
        system.debug(bodyString);
        if(bodyString != null){
            httpRequestObject.setBody(bodyString);
            httpRequestObject.setHeader('Content-Type', 'application/json');
        }
       /* String authorizationheadr='';
        if(Label.IsProduction.equalsIgnoreCase('Yes')){
            authorizationheadr='NLAuth nlauth_account='+Label.netsuite_Productio_Account+',nlauth_e-mail='
                +Label.Netsuite_Production_Username+',nlauth_signature='+Label.Netsuite_Production_Password;
            
        }
        else{
            authorizationheadr='NLAuth nlauth_account='+Label.netsuite_AccountId+',nlauth_e-mail='
                +Label.Netsuite+',nlauth_signature='+Label.Netsuite_Password;
            
        }
        system.debug('aaaa'+authorizationheadr);
        httpRequestObject.setHeader('Authorization',authorizationheadr);*/
        Http httpO=new Http();
        HttpResponse httpResp=httpO.send(httpRequestObject);
        System.debug(httpResp.getBody());
        return httpResp;
    }
}