import { LightningElement,api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import QUESTION_FIELD from '@salesforce/schema/Question__c.Question_Text__c';
import { NavigationMixin } from 'lightning/navigation';

export default class AskQuestionModel extends NavigationMixin(LightningElement) {

    @api objectApiName='Question__c';

    fields = [QUESTION_FIELD];

    handleSuccess(event) {
        const evt = new ShowToastEvent({
            title: 'Question is created',
            message: 'Record ID: ' + event.detail.id,
            variant: 'success',
        });
        this.dispatchEvent(evt);
        this.movetoPreviousPage();
    }

    movetoPreviousPage(){
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'posts'
            }
        });
    }

    closeModel(){
        this.movetoPreviousPage();
    }

}