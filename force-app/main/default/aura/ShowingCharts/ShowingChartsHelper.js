({
    
    createLineGraph : function(cmp, event) {
        var ctx = cmp.find('lineChart').getElement().getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                title: {
                            display: true,
                            text: 'Line Chart Data'
                        }
            }
        });
    },
    createRadarGraph:function(cmp,event){
        var ctx=cmp.find('radarChart').getElement().getContext('2d');
        var myChart=new Chart(ctx,{
            
            type: 'radar',
            data: {
                labels: [
                    'Eating',
                    'Drinking',
                    'Sleeping',
                    'Designing',
                    'Coding',
                    'Cycling',
                    'Running'
                ],
                datasets: [
                    {
                        label: 'My First Dataset',
                        data: [65, 59, 90, 81, 56, 55, 40],
                        fill: true,
                        backgroundColor: 'rgba(255, 99, 132, 0.2)',
                        borderColor: 'rgb(255, 99, 132)',
                        pointBackgroundColor: 'rgb(255, 99, 132)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgb(255, 99, 132)'
                    }, 
                    {
                        label: 'My Second Dataset',
                        data: [28, 48, 40, 19, 96, 27, 100],
                        fill: true,
                        backgroundColor: 'rgba(54, 162, 235, 0.2)',
                        borderColor: 'rgb(54, 162, 235)',
                        pointBackgroundColor: 'rgb(54, 162, 235)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgb(54, 162, 235)'
                    }
                ]
            },
            options: {
                elements: {
                    line: {
                        borderWidth: 3
                    }
                },
                title: {
                            display: true,
                            text: 'Radar Chart Data'
                        }
            }           
        });
    },
    
    createPolarAreaGraph:function(cmp,event){
        
        var ctx=cmp.find('polarArea').getElement().getContext('2d');
        console.log(ctx);
        var myChart=new Chart(ctx,{
            
            type: 'polarArea',
            data: {labels: [
                'Red',
                'Green',
                'Yellow',
                'Grey',
                'Blue'
            ],
                   datasets: [{
                       label: 'My First Dataset',
                       data: [11, 16, 7, 3, 14],
                       backgroundColor: [
                           'rgb(255, 99, 132)',
                           'rgb(75, 192, 192)',
                           'rgb(255, 205, 86)',
                           'rgb(201, 203, 207)',
                           'rgb(54, 162, 235)'
                       ]
                   }]},
            options: {title: {
                            display: true,
                            text: 'Polar Chart Data'
                        }}
        }
                             );
    }
})