public with sharing class IntegrationService1 implements IntegrationInterface {
    public IntegrationService1() {

    }

    public List<SObject> getDetail(){
        return [select id from contact  with security_enforced limit 1];
    }
}