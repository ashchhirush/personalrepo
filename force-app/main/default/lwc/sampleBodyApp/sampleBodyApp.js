import { LightningElement } from 'lwc';

export default class SampleBodyApp extends LightningElement {

    currentMonth="November";

    previousMonth(){

        if(this.currentMonth=="January"){
            this.currentMonth="December";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="February"){
            this.currentMonth="January";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="March"){
            this.currentMonth="February";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="April"){
            this.currentMonth="March";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="May"){
            this.currentMonth="April";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="June"){
            this.currentMonth="May";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="July"){
            this.currentMonth="June";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }

        if(this.currentMonth=="August"){
            this.currentMonth="July";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;

        }
        if(this.currentMonth=="September"){
            this.currentMonth="August";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="October"){
            this.currentMonth="September";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="November"){
            this.currentMonth="October";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="December"){
            this.currentMonth="November";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }

    }

    nextMonth(){

        if(this.currentMonth=="January"){
            this.currentMonth="February";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="February"){
            this.currentMonth="March";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="March"){
            this.currentMonth="April";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="April"){
            this.currentMonth="May";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="May"){
            this.currentMonth="June";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="June"){
            this.currentMonth="July";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="July"){
            this.currentMonth="August";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }

        if(this.currentMonth=="August"){
            this.currentMonth="September";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="September"){
            this.currentMonth="October";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="October"){
            this.currentMonth="November";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="November"){
            this.currentMonth="December";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }
        if(this.currentMonth=="December"){
            this.currentMonth="January";
            this.template.querySelector('c-calander-days-wrapper').fillDaysArray(this.currentMonth,'2022');
            return;
        }

    }


}