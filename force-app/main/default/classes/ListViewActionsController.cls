public class ListViewActionsController {
    
    @auraEnabled
    public static  List<wrapperClass> getListData(){
        List<wrapperClass> wpList=new list<wrapperClass>();
        for(Integer i=0;i<5;i++){
            wrapperClass wp=new wrapperClass();
            wp.s1='ABC'+i;
            wp.s2='DEF'+i;
            wp.s3='GHI'+i; 
            wp.s4='JKL'+i;
            wp.s5='MNO'+i;
            if(i==4){
                wp.emaild='as@gmail.com';
            }
            wpList.add(wp);
        }
        return wpList;
    }
    
    public class wrapperClass{
        @auraEnabled public string s1;
        @auraEnabled public string s2;
        @auraEnabled public string s3;
        @auraEnabled public string s4;
        @auraEnabled public string s5;   
         @auraEnabled public string emaild; 
    }
    
}