import { LightningElement } from 'lwc';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import FIRSTNAME from '@salesforce/schema/Contact.FirstName';
import LASTNAME from '@salesforce/schema/Contact.LastName';
import EMAIL from '@salesforce/schema/Contact.Email';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class ContactCreator extends LightningElement {

    fields=[FIRSTNAME,LASTNAME,EMAIL];
    objectApiName=CONTACT_OBJECT;

    handleSuccess(event){

        const toastEvent=new ShowToastEvent({
            title:'Contact created',
            message:'Record Id'+event.detail.id,
            variant:'success'
        });

        this.dispatchEvent(toastEvent);

    }
}