({
   scriptsLoaded : function(component, event, helper) {
        var action = component.get("c.getAllTasksByStatus");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                let val = response.getReturnValue() ;
                var labelset=[] ;
                var dataset=[] ;
                val.forEach(function(key) {
                    labelset.push(key.label) ; 
                    dataset.push(key.count) ; 
                });
                var ctx = component.find('pie-chart').getElement().getContext('2d');
                new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels:labelset,
                        datasets: [{
                            label: "Count of Task",
                            backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
                            data: dataset
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'Total Tasks by Status'
                        }
                    }
                });
            }     
        });
        $A.enqueueAction(action);
        helper.createLineGraph(component,event);
       helper.createRadarGraph(component,event);
       helper.createPolarAreaGraph(component,event);
    }
})