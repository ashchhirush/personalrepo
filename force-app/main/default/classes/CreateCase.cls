public class CreateCase {
    public static void createdata(){
        CaseTeamRole ctr=[select Id,name from CaseTeamRole where name='testread2'];
        Account acc = new Account(Name = 'Test Account1',Industry='Agriculture');
        insert acc;
        Case c1 = new Case();
        c1.AccountId=acc.id;
        Insert c1;
        Case c2 = new Case();
        c2.AccountId=acc.id;
        Insert c2;
        CaseTeamMember ctm=new CaseTeamMember() ;
        ctm.MemberId=UserInfo.getUserId();
        ctm.ParentId=c1.id;
        ctm.TeamRoleId=ctr.id;
     	insert ctm;        
        CaseTeamMember ctm2=new CaseTeamMember() ;
        ctm2.MemberId=UserInfo.getUserId();
        ctm2.ParentId=c2.id;
        ctm2.TeamRoleId=ctr.id;
     	insert ctm2;        
    }

}