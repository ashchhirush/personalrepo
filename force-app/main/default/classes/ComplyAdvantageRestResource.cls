@RestResource(urlMapping='/updateEventttt')
global with sharing class ComplyAdvantageRestResource {
    
    @HttpPost
    global static string updataInfo() {
        RestRequest req = RestContext.request;
        system.debug('Request' + (req.requestBody).tostring());
        Map<String,Object> parsedJson = (Map<String,Object>) JSON.deserializeUntyped((req.requestBody).tostring());
        system.debug('parsedJson--'+parsedJson);
        String event = (String)parsedJson.get('event');
        system.debug('getEvent--'+event);
        
        if(event=='match_status_updated'){
            Map<String,Object> data = (Map<String,Object>)parsedJson.get('data');
            String client_ref=(String)data.get('client_ref');
            Integer search_id=(Integer)data.get('search_id');
            String entity_id=(String)data.get('entity_id');
            String entity_match_status=(String)data.get('entity_match_status');
            Boolean is_whitelisted=(Boolean)data.get('is_whitelisted');
            Map<String,Object> tags=(Map<String,Object>)data.get('tags');
            system.debug(client_ref);
            system.debug(search_id);
            system.debug(entity_id);
            system.debug(entity_match_status);
            system.debug(is_whitelisted);
            system.debug(tags);
            
            
        }
        else if(event=='search_status_updated'){
            system.debug('Ia am here');
            Map<String,Object> data = (Map<String,Object>)parsedJson.get('data');
            String client_ref=(String)data.get('client_ref');
            Integer search_id=(Integer)data.get('search_id');
            String ref=(String)data.get('ref');
            Map<String,Object> changes=(Map<String,Object>)data.get('changes');
            Map<String,Object> before=(Map<String,Object>)changes.get('before');
            Integer beforeAssigned_to=(Integer)before.get('assigned_to');
            String beforeRisk_level=(String)before.get('risk_level');
            String beforeMatch_status=(String)before.get('match_status');
            Map<String,Object> after=(Map<String,Object>)changes.get('after');
            Integer afterAssigned_to=(Integer)after.get('assigned_to');
            String afterRisk_level=(String)after.get('risk_level');
            String afterMatch_status=(String)after.get('match_status');
            List<Object> termObj=(List<Object>)data.get('terms');
            List<Terms> terms=new List<Terms>();
            if(termObj!=null && !termObj.isEmpty()){
                for(integer t=0;t<termObj.size();t++){
                    Map<String,Object> term=(Map<String,Object>)termObj[t];
                    Terms tm=new Terms();
                    tm.name=(String)term.get('name');
                    tm.hits=(Integer)term.get('hits');
                    terms.add(tm);
                }
                
            }
            
            Map<String,Object> filterss = (Map<String,Object>)data.get('filters');
            if(filterss!=null && !filterss.isEmpty()){
                system.debug('Ia am here');
                Integer birthYear = (Integer)filterss.get('birth_year');
                List<object> allCountry=(List<object>)filterss.get('country_codes');
                List<String>countryCodes=new List<String>();
                if(allCountry!=null && !allCountry.isEmpty()){
                    for(Object o :allCountry){
                        countryCodes.add((string)o);
                    }
                }
                string entityType=(string)filterss.get('entity_type');
                Integer removeDeceased = (Integer)filterss.get('remove_deceased');
                List<object> alltypes=(List<object>)filterss.get('types');
                List<String>types=new List<String>();
                if(alltypes!=null && !alltypes.isEmpty()){    
                    for(Object t :alltypes){
                        types.add((string)t);
                    }
                }
                boolean exactMatch=(boolean)filterss.get('exact_match');
                Decimal fuzziness = (Decimal)filterss.get('fuzziness');
                system.debug('fuzziness--'+fuzziness);
            }
            
        }
        else if(event=='monitored_search_updated'){
            
            Map<String,Object> data = (Map<String,Object>)parsedJson.get('data');
            Integer search_id=(Integer)data.get('search_id');
            List<Object> allupdated=(List<Object>)data.get('updated');
            List<String> updated=new List<String>();
            if(allupdated!=null&& !allupdated.isEmpty()){
                for(Object au:allupdated){
                    updated.add((String)au);
                }
            }
            List<Object> allnew=(List<Object>)data.get('new');
            List<String> newid=new List<String>();
            if(allnew!=null && !allnew.isempty()){
                for(Object an:allnew){
                    newid.add((String)an);
                }
            }
            List<Object> allremoved=(List<Object>)data.get('removed');
            List<String> removed=new List<String>();
            if(allremoved!=null && !allremoved.isEmpty()){
                for(Object ar:allremoved){
                    removed.add((String)ar);
                }
            }
            Boolean is_suspended=(Boolean)data.get('is_suspended');    
        }
        
        return 'success';
    }
    
    public class Terms{
        public string name;
        public Integer hits;
    }
    
}