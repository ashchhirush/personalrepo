global class SubBatchApex implements Database.Batchable<sObject>, Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
         
        String query ='SELECT Id,User__c,Subscription_Membership__c,Subscription_Status__c,Subscription_Type__c,Platform__c from Subscription__c';
        return Database.getQueryLocator(query);
    }
      List<String> mat=new List<String>();
    global void execute(Database.BatchableContext BC, List<Subscription__c> subList) {
    	 
      // List<String> mat=new List<String>();
        List<Subscription__c> uniqSub=new List<Subscription__c>();
        for(Subscription__c sub:subList){
            String plat=sub.Platform__c;
            String subType=sub.Subscription_Type__c;
            String submem=sub.Subscription_Membership__c;
            String subuser=sub.User__c;
            String s=plat+subType+submem+subuser;
            if(mat.contains(s)){
                   
               
                uniqSub.add(sub);
               
            }
            else{
              
                mat.add(s);
            }
           	
        }
     	
       	
        try {
            // Update the subscription Record
          
             delete uniqSub;
      
        } catch(Exception e) {
            System.debug(e);
        }
         
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }
}