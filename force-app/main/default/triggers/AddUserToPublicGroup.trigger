trigger AddUserToPublicGroup on User (after insert) {
  
    if (Trigger.isAfter) {
        if (Trigger.IsInsert){
            
            AddUserToPublicGroupHandler.newUser(trigger.new);
        }
    }
}