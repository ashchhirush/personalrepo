import { LightningElement } from 'lwc';

export default class HelloWord2 extends LightningElement {

    newInput='';
    showData=false;
    newData(event){
        this.newInput=event.target.value;
    }
    checkbocClicked(event){
        this.showData=event.target.checked;
    }
}