public class Testingfuturecallouts {
    
    @Future(callout=true)
    public static void myfuture(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        system.debug(response);
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            // Cast the values in the 'animals' key as a list
            List<Object> animals = (List<Object>) results.get('animals');
            System.debug('Received the following animals:');
            for (Object animal: animals) {
                System.debug(animal);
            }
        }
        HttpResponse response1 = http.send(request);
       HttpResponse response2 = http.send(request);
        HttpResponse response3 = http.send(request);
        HttpResponse response4 = http.send(request);
    
    }
    public static void callingfuture(){
        for(integer i=0;i<50;i++){
            Testingfuturecallouts.myfuture();
        }
    }

}