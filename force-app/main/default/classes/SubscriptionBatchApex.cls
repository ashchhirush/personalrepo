global class SubscriptionBatchApex implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
         
        String query ='SELECT Id, Name,User__c,Type__C,End_Date__C,Start_Date__c,Subscription_Time__c from Subscription__c';
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<Subscription__C> subList) {
        
        // process each batch of records default size is 200
        List<Subscription__c> uniqSub=new List<Subscription__c>();
        //System.debug('dfsdf-----'+subList);
        for(Subscription__c sub : subList) {
            System.debug('----in first loop:'+sub);
            String contId=sub.User__c;
            String subtype=sub.Type__c;
            if(uniqSub.Size()==0){
                System.debug('It is first record so entered'+sub);
                uniqSub.add(sub);
            }
            for(Subscription__c uniqs:uniqSub){
                //compare with the list already sorted
                if(contId==uniqs.User__c && subtype==uniqs.Type__c){
                    //match according to type as well
                    if(sub.Start_Date__c > uniqs.Start_Date__c && sub.Start_Date__c<uniqs.End_Date__c){
                        //increment subscription if date lies in between
                        System.debug('----In date maching if'+sub.Start_Date__c+'---'+ uniqs.Start_Date__c+'---'+sub.Start_Date__c+'---'+uniqs.End_Date__c);
                        Integer subyear=(integer)sub.Subscription_Time__c;
                        uniqs.End_Date__c.addYears(subYear);
                    }
                }
                else{
                    uniqSub.add(sub);
                }
                
            }
        }
        try {
            // Update the subscription Record
           delete subList; 
            insert uniqSub;
         
        } catch(Exception e) {
            System.debug(e);
        }
         
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }
}