public class DemoCompare implements comparable{
    
    public long id;
    public string name;
    public string phone;
    
    public DemoCompare(long id,string name,string phone){
        this.id=id;
        this.name=name;
        this.phone=phone;
    }
    
    public integer compareTo(Object compareto){
        DemoCompare dc=(DemoCompare)compareto;
        if(id==dc.id) return 0;
        if(id>dc.id) return 1;
        return 0;
    }
    
    

}