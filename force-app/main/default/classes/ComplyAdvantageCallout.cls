public class ComplyAdvantageCallout 
{
    
    public ComplyAdvantageResponse cr{get;set;}
    public ComplyAdvantageCallout(){
        
        
        String strjson = ApexPages.currentPage().getParameters().get('response');
        system.debug(strjson);
        cr= ComplyAdvantageCallout.parseResponse(strjson);
        
      /* String attID = ApexPages.currentPage().getParameters().get('response');
        system.debug('Iam here in con'+attID);
        if(attID!=null&& string.isNotBlank(attID)){ 
       		Document at=[SELECT Id, Name,body FROM Document where id=:attID];
            if(at!=null){
            	system.debug('attttt'+at.id+at.name+at.Body);
       			String strjson2=at.body.tostring();
       			cr= ComplyAdvantageCallout.parseResponse(strjson2);
            }
        }*/
    }
    
    
    @future(callout = true)
    public static void doCallout(string csId,string typ)
    {
        String JsonRequest;
        String strjson;
        Case cse=[select id,Accountid,Contactid  from case where id=:csId];             
        if(typ=='Account'&& string.isNotBlank(cse.Accountid)){
            Account acc=[select id,name from account where id=:cse.Accountid];
            system.debug(acc.name);
            JsonRequest = getJsonRequest('A Bad Company',acc.id,null);
        }
        else if(typ=='Contact'&& string.isNotBlank(cse.ContactId)){
            Contact con=[select id,name,Birthdate from contact where id=:cse.ContactId];
            String birthd='';
            if(con.Birthdate!=null)
            	birthd=string.valueOf((con.Birthdate).year());
            JsonRequest = getJsonRequest('A test contact',con.id,birthd);
        }
        
        string ServiceEndpoint = 'https://api.complyadvantage.com/searches';
        Http http = new Http();
        Httprequest request= new Httprequest();
        request.setEndpoint(ServiceEndpoint);
        request.setTimeout(2 * 60 * 1000);
        request.setMethod('POST');
        request.setHeader('cache-control', 'no-cache' );
        request.setHeader('Content-Type', 'application/json;charset=UTF-8' );
        request.setHeader('Authorization', 'Token QfU4cxSfOOo1K0MuVznBu5nivPcvpuPe');
        request.setBody(jsonrequest);       
        HttpResponse response;
        try{
       		//ComplyAdvantageResponse complyResponse;
        	response = http.send(request);
        	if(response.getStatusCode() == 200)
        	{
            	strjson = response.getbody();
            	//complyResponse = parseResponse(strjson); // will be uncommented if some operation need to perform
            	//Attaching PDF
            	system.debug('strjsonghgh***'+strjson);
                
                /*Attachment cmplyAttach1 = new Attachment();
            	cmplyAttach1.ParentId = cse.id;
            	cmplyAttach1.name = 'Cghhg.txt';
            	cmplyAttach1.body = Blob.valueOf(strjson);
            	insert cmplyAttach1;
                string cpT=(string)cmplyAttach1.id;
                system.debug('sdfdsfdsf'+cpT);
                Attachment at=[SELECT Id, Name,body FROM Attachment where id=:cpT];
                system.debug(at.name);
                */
                
                
            	Attachment cmplyAttach = new Attachment();
            	cmplyAttach.ParentId = cse.id;
            	cmplyAttach.name = 'Comply'+typ+'.pdf';
            	PageReference myPdf = new PageReference('/apex/ComplyAdvantageResponsePDF');
            	myPdf.getParameters().put('response',strjson);
            	cmplyAttach.body = myPdf.getContentAsPdf();//blob.valueOf('hk');//
            	insert cmplyAttach;
        	}
        }
            catch(Exception ex){
                System.debug('**Callout Error***'+ex.getmessage());
            }
    }
    
    public static string getJsonRequest(string search_term,String client_ref,String birth_year)
    {        
        Filters objfilter= new Filters();
		objfilter.birth_year = birth_year;
        ComplyAdvantageRequest objJsonRequest = new ComplyAdvantageRequest();
        objJsonRequest.search_term = search_term;
      	objJsonRequest.client_ref =client_ref ;
		objJsonRequest.filters = objfilter;
		objJsonRequest.share_url = 1;
        string JsonRequest = JSON.serialize(objJsonRequest);
        if(string.isNotBlank(JsonRequest))
        {
            return JsonRequest;
        }
        else 
        {
            return null;
        }
    }
    
    public static ComplyAdvantageResponse parseResponse(String strjson)
    {
       	Map<String,Object>	parsedJson = (Map<String,Object>) JSON.deserializeUntyped(strjson);
        Map<String,Object> getData = (Map<String,Object>)parsedJson.get('content');
        Map<String,Object> stringvsValue = (Map<String,Object>)getData.get('data');
        Integer searchId = (Integer)stringvsValue.get('id');
        string reference = (string)stringvsValue.get('ref');
        Integer searcherId = (Integer)stringvsValue.get('searcher_id');
        Integer assigneeId = (Integer)stringvsValue.get('assignee_id');
        string matchStatus = (string)stringvsValue.get('match_status');
        string riskLevel = (string)stringvsValue.get('risk_level');
        string searchTerm = (string)stringvsValue.get('search_term');
        string submittedTerm = (string)stringvsValue.get('submitted_term');
        string clientRef = (string)stringvsValue.get('client_ref');
        Integer totalHits=(Integer)stringvsValue.get('total_hits');
        string updatedAt = (string)stringvsValue.get('updated_at');
        string createdAt = (string)stringvsValue.get('created_at');
        Integer lim = (Integer)stringvsValue.get('limit');
        Integer offSet = (Integer)stringvsValue.get('offset');
        string shareUrl=(string)stringvsValue.get('share_url');
        
        //Date related to filters
        Map<String,Object> filterss = (Map<String,Object>)stringvsValue.get('filters');
        if(filterss!=null && !filterss.isEmpty()){
            Integer birthYear = (Integer)filterss.get('birth_year');
            string entityType=(string)filterss.get('entity_type');
            Integer removeDeceased = (Integer)filterss.get('remove_deceased');
            boolean exactMatch=(boolean)filterss.get('exact_match');
            Decimal fuzziness = (Decimal)filterss.get('fuzziness');
            List<object> allCountry=(List<object>)filterss.get('country_codes');
            List<String>countryCodes=new List<String>();
            if(allCountry!=null && !allCountry.isEmpty()){
                for(Object o :allCountry){
                    countryCodes.add((string)o);
                }
            }
            System.debug(countryCodes);
            List<object> alltypes=(List<object>)filterss.get('types');
            List<String>types=new List<String>();
            if(alltypes!=null && !alltypes.isEmpty()){
                for(Object t :alltypes){
                    types.add((string)t);
                }
            }
        }
        
        //Data related to searcher
        Integer searcherId2;
        string searcherEmail;
        string searcherName;
        string searcherPhone;
        string searcherCreated_at;
        boolean searcherUser_is_active;
        Map<String,Object> searcher = (Map<String,Object>)stringvsValue.get('searcher');
        if(searcher!=null && !searcher.isEmpty()){
            searcherId2 = (Integer)searcher.get('id');
            searcherEmail=(string)searcher.get('email');
            searcherName=(string)searcher.get('name');
            searcherPhone=(string)searcher.get('phone');
            searcherCreated_at=(string)searcher.get('created_at');
            searcherUser_is_active=(boolean)searcher.get('user_is_active');
        }
        
        //Date related to assignee
        Integer assigneeId2;
        string assigneeEmail;
        string assigneeName;
        string assigneePhone;
        string assigneeCreated_at;
        boolean assigneeUser_is_active;
        Map<String,Object> assignee = (Map<String,Object>)stringvsValue.get('assignee');
        if(assignee!=null && !assignee.isEmpty()){
            assigneeId2 = (Integer)assignee.get('id');
            assigneeEmail=(string)assignee.get('email');
            assigneeName=(string)assignee.get('name');
            assigneePhone=(string)assignee.get('phone');
            assigneeCreated_at=(string)assignee.get('created_at');
            assigneeUser_is_active=(boolean)assignee.get('user_is_active');
        } 
        
        //Data related to hits
        List<Object> hits=(List<Object>)stringvsValue.get('hits');
        List<Hit> allHits=new List<Hit>();       //list containg data of all hits
        for(Integer i=0;i<hits.size();i++){
            Map<String,Object> dList=(Map<String,Object>)hits[i];            
            Map<String,object> docData=(Map<String,object>)dList.get('doc');
            Hit ht=new Hit();
            ht.entity_type=(String)docData.get('entity_type');
            ht.id=(String)docData.get('id');
            ht.last_updated_utc=(String)docData.get('last_updated_utc');
            ht.name=(String)docData.get('name');
            ht.score=(decimal)dList.get('score');
            ht.match_status=(String)dList.get('match_status');
            ht.is_whitelisted=(Boolean)dList.get('is_whitelisted');  
            List<Object> allmatchType=(List<Object>)dList.get('match_types');
            ht.match_types=new list<string>();
            if(allmatchType!=null && !allmatchType.isEmpty()){
                for(Object mt:allmatchType){
                    ht.match_types.add((string)mt);    
                }
            }
            
            List<Object> alltypes=(List<Object>)docData.get('types');
            ht.types=new list<string>();
            if(alltypes!=null && !alltypes.isEmpty()){
                for(Object t:alltypes){
                    ht.types.add((string)t);    
                }
            }
            List<object> allsources=(List<object>)docData.get('sources');
            ht.sources=new list<string>();
            if(allsources!=null && !allsources.isEmpty()){
                for(Object s:allsources){
                    ht.sources.add((string)s);    
                }
            }
            //Parsing Media
            List<object> mediaObj=(List<Object>)docData.get('media');
            List<Media> allMedia=new list<Media>();
            if(mediaObj!=null && !mediaObj.isEmpty()){
                for(integer m=0;m<mediaObj.size();m++){
                    Map<String,Object> media=(Map<String,Object>)mediaObj[m]; 
                    Media md=new Media();
                    md.dated=(String)media.get('date');
                    md.snippet=(String)media.get('snippet');
                    md.title=(String)media.get('title');
                    md.url=(String)media.get('url');
                    allMedia.add(md);                
                }
            }
            ht.media=allMedia;
            
            //Parsing Fields        
            List<object> fieldObj=(List<Object>)docData.get('fields');
            List<Fields> allfields=new list<Fields>();
            if(fieldObj!=null && !fieldObj.isEmpty()){
                for(integer f=0;f<fieldObj.size();f++){
                    Map<String,Object> field=(Map<String,Object>)fieldObj[f]; 
                    Fields fd=new Fields();
                    fd.name=(string)field.get('name');
                    fd.source=(string)field.get('source');
                    fd.value=(string)field.get('value');
                    allfields.add(fd);                
                }
            }
            ht.fields=allfields;
            
            //Parsing Aka
            List<object> akaObj=(List<Object>)docData.get('aka');
            List<Aka> allakas=new list<Aka>();
            if(akaObj!=null && !akaObj.isEmpty()){
                for(integer a=0;a<akaObj.size();a++){
                    Map<String,Object> aka=(Map<String,Object>)akaObj[a]; 
                    Aka ak=new Aka();
                    ak.name=(string)aka.get('name');
                    allakas.add(ak);                
                }
            }
            ht.aka=allakas;
            
            //Parsing Assets
            List<object> assetsObj=(List<Object>)docData.get('assets');
            List<Assets> allassets=new list<Assets>();
            if(assetsObj!=null && !assetsObj.isEmpty()){
                for(integer at=0;at<assetsObj.size();at++){
                    Map<String,Object> asset=(Map<String,Object>)assetsObj[at]; 
                    Assets ats=new Assets();
                    ats.public_url=(string)asset.get('public_url');
                    ats.source=(string)asset.get('source');
                    ats.type1=(string)asset.get('type');
                    allassets.add(ats);                
                }
            }
            ht.assets=allassets;
            
            allHits.add(ht);
            
        }
        
        
        ComplyAdvantageResponse cr = new ComplyAdvantageResponse();
        cr.id=searchId;     
        cr.ref=reference;
        cr.searcher_id=searcherId;
        cr.assignee_id=assigneeId;
        cr.match_status=matchStatus;
        cr.risk_level=riskLevel;
        cr.search_term=searchTerm;
        cr.submitted_term=submittedTerm;
        cr.client_ref=clientRef;
        cr.updated_at=updatedAt;
        cr.created_at=createdAt;
        cr.share_url=shareUrl;
        cr.total_hits=totalHits;
        cr.limit1=lim;
        cr.offset=offSet;
        cr.searcherID=searcherId2;
        cr.searcheremail=searcherEmail;
        cr.searchername=searcherName;
        cr.searcherphone=searcherPhone;
        cr.searchercreated_at=searcherCreated_at;
        cr.searcheruser_is_active=searcherUser_is_active;
        cr.assigneeID=assigneeId2;
        cr.assigneeemail=assigneeEmail;
        cr.assigneename=assigneeName;
        cr.assigneephone=assigneePhone;
        cr.assigneecreated_at=assigneeCreated_at;
        cr.assigneeuser_is_active=assigneeUser_is_active;
        
        cr.hits=allHits;
        
        return cr;
    }
    
    public class Hit{
        
          public string entity_type{get;set;}
           public string id{get;set;}
          public string last_updated_utc{get;set;}
          public string name{get;set;}
          public decimal score{get;set;}
           public string match_status{get;set;}
           public boolean is_whitelisted{get;set;}
           public list<String> match_types{get;set;}
           public list<String> types;
           public list<String> sources;
           public List<Media> media;
          public list<Fields> fields;
           public list<Aka> aka;
           public list<Assets> assets;
    }
    public class Media{
        public string dated;
        public string snippet;
        public string title;
        public string url;   
    }
    public class Fields{
        public string name;
        public string source;
        public string value;
    }
    public class Aka{
        public string name;
    }
    public class Assets{
        public string public_url;
        public string source;
        public string type1;
    }
    
    
    public class ComplyAdvantageResponse{
        public integer id{get;set;}
          public string ref{get;set;}
        public integer searcher_id{get;set;}
        public integer assignee_id{get;set;}
     public string match_status{get;set;}
        public string risk_level{get;set;}
         public string search_term{get;set;}
        public string submitted_term{get;set;}
        public string client_ref{get;set;}
         public string updated_at{get;set;}
       public string created_at{get;set;}
         public string share_url{get;set;}
        public integer total_hits{get;set;}
        public integer limit1{get;set;}
         public integer offset{get;set;}
         public integer searcherID{get;set;}
          public string searcheremail{get;set;}
          public string searchername{get;set;}
          public string searcherphone{get;set;}
          public string searchercreated_at{get;set;}
          public boolean searcheruser_is_active{get;set;}
          public integer assigneeID{get;set;}
          public string assigneeemail{get;set;}
          public string assigneename{get;set;}
          public string assigneephone{get;set;}
          public string assigneecreated_at{get;set;}
          public boolean assigneeuser_is_active{get;set;}
          public List<Hit> hits{get;set;}
        
    }
    
    
    public class ComplyAdvantageRequest
    {
        public String search_term;
       	public String client_ref;
		public Filters filters;
		public Integer share_url;
    }
    public class Filters 
	{
		public String birth_year;
	}
}