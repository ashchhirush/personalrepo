trigger OpportunityLineItrmTrigger on OpportunityLineItem (after delete) {

    Set<id> optyIds=new set<id>();
    for(OpportunityLineItem oLI:trigger.old){
        optyIds.add(oLI.opportunityId);
    }

    delete [select id from Opportunity where id in:optyIds];

}